call plug#begin('~/.vim/plugged')

Plug 'ErichDonGubler/vim-sublime-monokai'
Plug 'Lokaltog/vim-distinguished'
Plug 'stevearc/vim-arduino'

call plug#end()
set number
let NERDTreeWinPos = 'right'
nnoremap <C-e> :NERDTreeToggle<CR>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
